<?php

require_once "Response.php";

function checkRegex($data, Response &$response){
    $regex['login'] = '#\w{6,}#';
    $regex['psswd'] = '/(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}/';
    $regex['email'] = '/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u';
    $regex['name'] = '/^[A-Za-z0-9]{2}$/';

    $errors_text['login'] = 'логин должен содержать минимум 6 символов, буквы или цифры';
    $errors_text['psswd'] = 'минимум 6 символов , обязательно должны содержать цифру, буквы в разных регистрах и спец символ (знаки)';
    $errors_text['email'] = 'не подходит под валидацию email';
    $errors_text['name'] = '2 символа, только буквы и цифры';


    for( reset($data); ($key = key($data)); next($data)) {
        if(isset($regex[$key])) {
            if(!preg_match($regex[$key], $data[$key])) {
                $response->addError($key. '_error', $errors_text[$key]);
            }
        }
    }

}
