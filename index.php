<?php
require_once 'DB.php';
require_once 'User.php';

$base = new DB('base.xml');
if(isset($_COOKIE['my_session_id']))
    $MBuser = $base->getUserByField('session_id', $_COOKIE['my_session_id']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache">
    <title>Title</title>
    <link href="style.css" type="text/css" rel="stylesheet">
</head>
<body>
<script src="jquery-3-3-1-min.js"></script>
<script src="script.js"></script>
<?php if(!isset($_COOKIE['my_session_id']) || !$MBuser):?>
    <div class="wrapper">
        <form action="reg.php" method="post" id="registration_form">
            <h1>Форма регистрации</h1>
            <div>
                <label>login</label>
                <input type="text" name="login" id="reg_input" required>
                <p id="login_error"></p>
            </div>
            <div>
                <label>password</label>
                <input type="password" name="psswd" id="reg_input" required>
                <p id="psswd_error"></p>
            </div>
            <div>
                <label>confirm password</label>
                <input type="password" name="confirm_psswd" id="reg_input" required>
                <p id="confirm_psswd_error"></p>
            </div>
            <div>
                <label>email</label>
                <input type="email" name="email" id="reg_input" required>
                <p id="email_error"></p>
            </div>
            <div>
                <label>name</label>
                <input type="text" name="name" id="reg_input" required>
                <p id="name_error"></p>
            </div>
            <input type="submit" name="submit" value="Зарегистрироваться">
            <p id="status_reg"></p>
        </form>

        <div class="bd_button_wrapper">
            <a href="crud_page.php">
                <button class="bd_button">редактировать БД</button>
            </a>
        </div>

        <form action="login.php" method="post" id="login_form">
            <h1>Форма Входа</h1>
            <div>
                <label>login</label>
                <input type="text" name="login" required>
                <p id="login_error"></p>
            </div>
            <div>
                <label>password</label>
                <input type="password" name="psswd" required>
                <p id="psswd_error"></p>
            </div>
            <input type="submit" value="войти">
            <p id="status_login"></p>
        </form>
    </div>
<?php endif;?>
<?php if(isset($_COOKIE['my_session_id']) && $MBuser){
    session_start();
    $user = new User($MBuser);
    echo  'Hello ' . $user->asSXE()->name . "</br>";
} ?>
<?php if(isset($_COOKIE['my_session_id']) && $MBuser): ?>
<form method="post" action="logOut.php" id="logOut_form">
    <input type="submit" name="logOut" value="Выйти">
</form>
<?php endif;?>
</body>
</html>