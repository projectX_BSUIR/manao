<?php
require_once 'CRUD.php';
$crud = new CRUD('base.xml');
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>

<?php
    if(!isset($_GET['edit']) && !isset($_GET['add'])){
        $crud->getFullTable()->render();
    }
?>

<?php
if(isset($_GET['edit'])){
    $id = htmlspecialchars($_GET['edit']);
    $crud->getEditPage($id)->render();
}
?>


<?php
if(isset($_GET['add'])){
    $crud->getAddPage()->render();
}
?>
</body>
</html>
