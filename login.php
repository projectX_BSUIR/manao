<?php
 require_once "DB.php";
require_once "User.php";
require_once "Response.php";


////start////проверка на ajax

$isAjax = false ;
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $isAjax = true;
}
if(!$isAjax)
    exit();

if(!isset($_POST['login']) || !isset($_POST['psswd'])) {
    exit();
}

//////end////////

$response = new Response();//создание объекта для ответа на страницу

$data['login'] = trim(htmlspecialchars($_POST['login']));
$data['psswd'] = trim(htmlspecialchars($_POST['psswd']));


$base = new DB('base.xml');

if($base->create_error) {////если произошла обшика открытия базы данных
    $response->addError('status_login', 'произошла ошибка на сервере');
    echo $response->asJSON();
    exit();
}

//////start/////проверка на существование пользователя

$MBuser = $base->getUserByField('login', $data['login']);
if(!$MBuser) {
    $response->addError('login_error', 'не существует такого логина');
    echo $response->asJSON();
    exit();
}else{
    $user = new User($MBuser);
}

////проверка совпадения введенного пароля и хранящегося в базе данных пароля

if($user->checkPassword($data['psswd']))
    $user->logIn($base);
else $response->addError('psswd_error', 'пароль неверный');

echo $response->asJSON();