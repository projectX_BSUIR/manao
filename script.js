jQuery(function ($) {
    $("#registration_form").submit(function(e)
    {
        e.preventDefault();
        register();
    })
    $("#login_form").submit(function(e){
        e.preventDefault();
        login();
    })
    $("#logOut_form").submit(function(e){
        e.preventDefault();
        logOut();
    })
})

function register() {
    let PostData = $('#registration_form').serialize();

    let elements = $("#registration_form div").children("p");
    for(key in elements){
        if(elements.hasOwnProperty(key)){
            elements[key].innerText = '';
        }
        $('#status_reg').text('');
    }

    $.ajax(
        {
            url: 'reg.php',
            type: 'post',
            dataType: 'json',
            data: PostData,
            success:function(data) {
                if(data.status == 'error'){
                    for(key in data[0]){
                        if(data[0].hasOwnProperty(key)){
                            $("#registration_form #" + key).text(data[0][key]);///вывод всех ошибок в нуные места
                        }
                    }
                }
                else{
                    $("#status_reg").text('регистрация успешно пройдена');
                }
            },
            error: function (data){
            }
        }
    )
}

function login(){
    let PostData = $('#login_form').serialize();

    let elements = $("#login_form div").children("p");
    for(key in elements){
        if(elements.hasOwnProperty(key)){
            elements[key].innerText = '';
        }
    }
    $.ajax(
        {
            url: 'login.php',
            type: 'post',
            dataType: 'json',
            data: PostData,
            success: function(data){
                if(data.status == 'error'){
                    for(key in data[0]){
                        if(data[0].hasOwnProperty(key)){
                            $("#login_form #" + key).text(data[0][key]);
                        }
                    }
                }else{
                    location.href ="index.php";
                }
            },
            error: function(data){
            }
        }
    )
}

function logOut(){
    $.ajax(
        {
            url: 'logOut.php',
            type: 'post',
            dataType: 'text',
            success: function(data){
                location.href = 'index.php';
            }
        }
    )
}