<?php
require_once "DB.php";

class User
{
    private SimpleXMLElement $user;

    public function __construct(SimpleXMLElement $user){
        $this->user = $user;
    }

    ///получени пользователя как SimpleXMLElement
    public function asSXE() : SimpleXMLElement
    {
        return $this->user;
    }


    //сверяет $psswd и пароль, хранящийся в базе данных
    //возвращает true, если пароли совпадают, false в обратном случае
    public function checkPassword($psswd)
    {
        if($this->user->psswd == md5($psswd . $this->user->sault)){
            return true;
        }else{
            return false;
        }
    }

        ////производит авторизацию пользователя
    /// возвращает количество записанных байт, false, если произошла ошибка
    public function logIn(DB &$base){
        session_start();
        setcookie('my_session_id', session_id() . md5($this->user->login), time() + 600);///утсанавливается кука, идетифицирующая рользователя, время 10 минут, момтоит из session_id и хэша логина
        /// если в одной сессии создается несколько пользователей, это добавит возможность их дифференцирования
        return $base->setField('session_id', session_id() . md5($this->user->login), $this->user);/////пеезаписывается кука пользователю
    }
}