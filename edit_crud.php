<?php

require_once 'CRUD.php';
require_once 'functions.php';
require_once 'Response.php';

$data['login'] = trim(htmlspecialchars($_POST['login']));
$data['email'] = trim(htmlspecialchars($_POST['email']));
$data['name'] = trim(htmlspecialchars($_POST['name']));
$data['id'] = trim(htmlspecialchars($_POST['id']));


$response = new Response();

checkRegex($data, $response);

if($response->hasErrors()){
    foreach ($response->getErrors() as $error)
        echo $error . "</br>";
    echo "<a href='crud_page.php?edit=" . $data['id'] . "'>повторите попытку ввода</a>";
    exit;
}

$crud = new CRUD('base.xml');

if(!$crud->isUserUnique($data['login'], $data['email'], $data['id'])){
    echo 'пользователь не уникален</br>';
    echo "<a href='crud_page.php?edit=" . $data['id'] . "'>повторите попытку ввода</a>";
    exit;
}

$user = $crud->getUserByField('id', $data['id']);

for(reset($data); ($key = key($data)); next($data)){/////?update полей записи
    $user->$key = $data[$key];
}
//сохранение изменений
$crud->saveBase();

header("Location: crud_page.php");