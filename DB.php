<?php


class DB
{
    protected $base;
    protected $src;
    public $create_error = false;

    public function __construct($src){
        $this->src = $src;
        $this->base = simplexml_load_file($this->src);
        if($this->base == false) {
            $this->create_error = true;
        }
    }
    ///генерирует радномную строку длины $length
    private function GenerateRandomString($length = 10){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //возвращает полследнего узера
    public function getLastUser(){
        if(isset($this->base->user[$this->base->count() - 1]))
            return $this->base->user[$this->base->count() - 1];
        else return 0;
    }

    //сохраняет изменения в базе данных
    public function saveBase(){
        $dom = new DOMDocument('1.0');
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $dom->loadXML($this->base->asXML());
        return $dom->save($this->src);
    }

    //добавляет пользователя в базу данных
    public function addUser($login, $email, $name, $psswd){
        $id = $this->getLastUser()->id + 1;
        $user = $this->base->addChild('user');
        $user->addChild('id', $id);
        $sault = $this->GenerateRandomString(20);//используется для создания "соли"
        $hash = md5($psswd . $sault);
        $user->addChild('login', $login);
        $user->addChild('email', $email);
        $user->addChild('name', $name);
        $user->addChild('psswd', $hash);
        $user->addChild('session_id', session_id() . md5($login));
        $user->addChild('sault', $sault);
        $this->saveBase();
    }

    //получение данных о пользователях, когда ключ не уникален и может быть несколько пользователей
    //возвращает массив пользователей, false если пользователей с таким значением поля н нашлось
    public function getUsersByField($field, $value){
        foreach ($this->base->user as $user) {
            if($user->$field == $value) {
                $result[] = $user;
            }
        }
        if(count($result) > 0)
            return $result;
        else return false;
    }

    //получение данных пользователя по уникальному ключу
    //возвращает SimpleXMLElement пользователя, false в случае ненахождения полльзователя
    public function getUserByField($field, $value){
        foreach ($this->base->user as $user) {
            if($user->$field == $value)
                return $user;
        }
        return false;
    }

    //проверяется уникальность пользователя в базе данных по login и email
    //возвращает true, если пользователь уникален, false в обратном случае
    public function isUserUnique($login, $email, $id = false){
        foreach($this->base->user as $user) {
            if(($user->login == $login || $user->email == $email) && $user->id != $id)
                return false;
        }
        return true;
    }

    //изменяет значение поля у конкретного $user, $field-имя поля для изменения, $value-новое значение поля
    //возращает количество записанных байт, false в случае ошибки
    public function setField($field, $value, SimpleXMLElement &$user){
        if(isset($user->$field))
            $user->$field = $value;
        else return false;
        return $this->saveBase();
    }

        //удаление пользователя из базы данных
    //возвращает //возращает количество удаленных байт, false в случае ошибки
    public function deleteUser($id){
        $user = $this->getUserByField('id', $id);
        if(!$user)
            return false;
        $dom = dom_import_simplexml($user);
        $dom->parentNode->removeChild($dom);
        return $this->saveBase();
    }


    public function __destruct(){
        unset($this->base);
        unset($this->src);
    }
}