<?php

require_once "DB.php";

class CRUD extends DB
{

    private $str;

    public function __construct($src)
    {
        parent::__construct($src);
    }

    public function getFullTable(){
        $this->str = '<div class="crud_wrapper">
                        <div>
                            <a href="crud_page.php?add=1">
                                <button class="add_crud_button crud_buttons">+</button>
                            </a>
                        </div>
                        <table class="crud_table">
                            <tr>
                                <td>id</td>
                                <td>name</td>
                                <td>email</td>
                                <td>login</td>
                                <td>password</td>
                            </tr>';
        foreach ($this->getUsers() as $user){
            $this->str .= '<tr>
                                <td>' . $user->id . '</td>
                                <td>' . $user->name . '</td>
                                <td>' . $user->email . '</td>
                                <td>' . $user->login . '</td>
                                <td>' . $user->psswd . '</td>
                                <td class="crud_buttons">
                                    <a href="crud_page.php?edit=' . $user->id . '">
                                        <button class="edit_crud_button">edit</button>
                                    </a>
                                </td>
                                <td class="crud_buttons">
                                    <a href="delete_crud.php?delete=' . $user->id . '">
                                        <button class="delete_crud_button">delete</button>
                                    </a>
                                </td>
                            </tr>';
        }
        $this->str .= '</table></div>';
        return $this;
    }

    public function getUsers(){
        foreach ($this->base->user as $user){
            yield $user;
        }
    }

    public function getEditPage($id){
        $user = $this->getUserByField('id', $id);
        $this->str = '<div class="edit_form_wrapper">
                        <form action="edit_crud.php" method="post" id="edit_form">
                            <label>Имя</label>
                            <input type="text" name="name" value="' . $user->name . '" required></br>
                            <label>Email</label>
                            <input type="text" name="email" value="' . $user->email . '" required></br>
                            <label>Логин</label>
                            <input type="text" name="login" value="' . $user->login . '" required></br>
                            <input type="hidden" name="id" value="' . $user->id . '">
                            <input type="submit" value="редактировать" class="edit_page_edit_button">
                        </form>
                     </div>';
        return $this;
    }

    public function getAddPage(){
        $this->str =
            '<div class="add_form_wrapper">
                <form action="add_crud.php" method="post" id="add_form">
                    <label>Имя</label>
                    <input type="text" name="name" required></br>
                    <label>Email</label>
                    <input type="text" name="email" required></br>
                    <label>Логин</label>
                    <input type="text" name="login" required></br>
                    <label>password</label>
                    <input type="text" name="psswd" required></br>
                    <input type="submit" value="добавить" class="add_page_add_button">
                </form>
            </div>';
        return $this;
    }

    public function render(){

        echo $this->str;
    }

    public function __destruct()
    {
        parent::__destruct();
    }
}