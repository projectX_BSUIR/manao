<?php
require_once 'CRUD.php';
$crud = new CRUD('base.xml');
if(isset($_GET['delete'])){
    $id = htmlspecialchars($_GET['delete']);
    if($crud->deleteUser($id))
        header('Location: crud_page.php');
    else{
        echo "такого пользователя не существует</br>";
        echo "<a href='crud_page.php'>повторите попытку</a>";
        exit;
    }
}
