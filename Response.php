<?php


class Response
{
    private $response;
    private $status;

    public function __construct(){
        $this->status = 'ok';
        $this->response[0] = [];
    }


    //добавляет ошибку
    public function addError($index, $message){
        $this->response[0][$index] = $message;
        $this->status = 'error';
    }

    ///конвертирует все добавленные данные в json-формат
    /// возвращает JSON объект
    public function asJSON(){
        $this->response['status'] = $this->status;
        return json_encode($this->response);
    }
    //возвращает true, если ошибки были добавлены, false, если ошбок не было
    public function hasErrors(){
        if($this->status == 'error')
            return true;
        else return false;
    }

    //генератор, возвращает тексты ошибок
    public function getErrors(){
        foreach ($this->response[0] as $error){
            yield $error;
        }
    }
}