<?php


require_once 'CRUD.php';
require_once 'Response.php';
require_once 'functions.php';

$data['login'] = trim(htmlspecialchars($_POST['login']));
$data['email'] = trim(htmlspecialchars($_POST['email']));
$data['name'] = trim(htmlspecialchars($_POST['name']));
$data['psswd'] = trim(htmlspecialchars($_POST['psswd']));


$response = new Response();

checkRegex($data, $response);

if($response->hasErrors()){
    foreach ($response->getErrors() as $error)
        echo $error . "</br>";
    echo "<a href='crud_page.php?add=1'>повторите попытку ввода</a>";
    exit;
}

$crud = new CRUD('base.xml');
if($crud->isUserUnique($data['login'], $data['email'])) {
    $crud->addUser($data['login'], $data['email'], $data['name'], $data['psswd']);
    header('Location: crud_page.php');
}else{
    echo "Юзер не уникален, логин и email должны быть уникальны <a href='crud_page.php?add=1'>Повторить попытку ввода</a>";
}
