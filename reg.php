<?php

require_once 'DB.php';
require_once 'Response.php';
require_once 'functions.php';
session_start();



///проверка на ajax
$isAjax = false;
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $isAjax = true;
}
if(!$isAjax)
    exit();

if(!isset($_POST['login']) || !isset($_POST['name']) || !isset($_POST['email']) || !isset($_POST['psswd']) || !isset($_POST['confirm_psswd'])) {
    exit();
}


$response = new Response();

$data['login'] = trim(htmlspecialchars($_POST['login']));
$data['psswd'] = trim(htmlspecialchars($_POST['psswd']));
$data['confirm_psswd'] = trim(htmlspecialchars($_POST['confirm_psswd']));
$data['email'] = trim(htmlspecialchars($_POST['email']));
$data['name'] = trim(htmlspecialchars($_POST['name']));

////проверка на соответствие введенных данных регуляркам(находятся в functions.php)
checkRegex($data, $response);


if($data['psswd'] != $data['confirm_psswd'])
    $response->addError('confirm_psswd_error', 'пароли не соответствуют');


if($response->hasErrors()){
    echo $response->asJSON();
    exit();
}



$base = new DB('base.xml');
if($base->create_error) {
    $response->addError('status_reg', 'произошла ошибка на сервере');
    echo $response->asJSON();
    exit();
}

if($base->isUserUnique($data['login'], $data['email'])) {
    $base->addUser($data['login'], $data['email'], $data['name'], $data['psswd']);
} else{
    $response->addError('status_reg', 'такой пользователь уже существует');
}

echo $response->asJSON();


